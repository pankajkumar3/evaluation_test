package com.pankaj.evaluation_test_project.views.signupActivity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.pankaj.evaluation_test_project.HomeActivity
import com.pankaj.evaluation_test_project.R
import com.pankaj.evaluation_test_project.databinding.ActivitySignupBinding
import kotlinx.android.synthetic.main.activity_signup.*

class SignUpActivity : AppCompatActivity() {
    private var signUp:ActivitySignupBinding?=null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        signUp = DataBindingUtil.setContentView(this, R.layout.activity_signup)
        val factory = SignUpFactory(this)
        val viewModel = ViewModelProvider(this, factory).get(SignUpVM::class.java)
        signUp?.viewModel=viewModel
        register()
    }
        fun register() {
            btnSignup.setOnClickListener {
                when {
                    TextUtils.isEmpty(edtName.text.toString().trim { it <= ' ' }) ->
                        Toast.makeText(this@SignUpActivity, "Please enter name", Toast.LENGTH_SHORT)
                            .show()
                    TextUtils.isEmpty(edtEmail.text.toString().trim { it <= ' ' }) ->
                        Toast.makeText(this@SignUpActivity, "Please enter email", Toast.LENGTH_SHORT)
                            .show()
                    TextUtils.isEmpty(edtPhone.text.toString().trim { it <= ' ' }) ->
                        Toast.makeText(this@SignUpActivity, "Please enter phone number", Toast.LENGTH_SHORT)
                            .show()
                    TextUtils.isEmpty(edtPassword.text.toString().trim { it <= ' ' }) ->
                        Toast.makeText(this@SignUpActivity, "Please enter password", Toast.LENGTH_SHORT)
                            .show()
                    TextUtils.isEmpty(edtConPassword.text.toString().trim { it <= ' ' }) ->
                        Toast.makeText(this@SignUpActivity, "Please enter confirm password", Toast.LENGTH_SHORT)
                            .show()
                    else -> {
                        //get values from edit_text convert it to string and stored in reference variable
                        val name: String = edtName.text.toString().trim { it <= ' ' }
                        val email: String = edtEmail.text.toString().trim { it <= ' ' }
                        val password: String = edtPassword.text.toString().trim { it <= ' ' }
                        //Initialize firebase auth with create user with(email ,password)
                        FirebaseAuth.getInstance().createUserWithEmailAndPassword(email, password)
                            .addOnCompleteListener { task ->
                                if (task.isSuccessful()) {
                                    //firebase registered user
                                    val firebaseUser: FirebaseUser = task.result!!.user!!
                                    Toast.makeText(this@SignUpActivity, "User registered successfully", Toast.LENGTH_SHORT)
                                            .show()
                                    val intent =
                                        Intent(this@SignUpActivity, HomeActivity::class.java)
                                    intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                                    intent.putExtra("user_name", name)
                                    intent.putExtra("user_id", firebaseUser.uid)
                                    intent.putExtra("email_id", email)
                                    startActivity(intent)
                                    finish()
                                    //exception throw when user not registered
                                } else {
                                    Toast.makeText(this@SignUpActivity, task.exception!!.message.toString(), Toast.LENGTH_SHORT)
                                            .show()
                                }
                            }
                    }
                }
            }
        }
        }

