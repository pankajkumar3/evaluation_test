package com.pankaj.evaluation_test_project.views.signupActivity

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class SignUpFactory(val context: Context):ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(SignUpVM::class.java)) {
            return SignUpVM(context) as T
        }
        throw IllegalArgumentException("")
    }
    }
