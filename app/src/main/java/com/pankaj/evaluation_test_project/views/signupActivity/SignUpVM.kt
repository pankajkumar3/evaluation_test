package com.pankaj.evaluation_test_project.views.signupActivity

import android.app.Activity
import android.content.Context
import android.content.Intent
import androidx.databinding.ObservableField
import androidx.lifecycle.ViewModel
import com.pankaj.evaluation_test_project.views.loginActivity.LoginActivity

class SignUpVM(val context: Context):ViewModel() {
    val name=ObservableField("")
    val phone=ObservableField("")
    val email=ObservableField("")
    val password=ObservableField("")
    val conPassword=ObservableField("")
    var selectedCode=ObservableField(91)
    fun clicks(value:String)
    {
        when(value)
        {
           "login"->
           {
               (context as SignUpActivity).startActivity(Intent(context,LoginActivity::class.java))
           }
        }
    }
}