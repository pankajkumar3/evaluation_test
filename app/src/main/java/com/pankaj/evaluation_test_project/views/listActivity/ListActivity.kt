package com.pankaj.evaluation_test_project.views.listActivity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.google.firebase.firestore.FirebaseFirestore
import com.pankaj.evaluation_test_project.R
import kotlinx.android.synthetic.main.activity_list.*

class ListActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list)
        //calling write function
        write()
    }
    fun write()
    {

        //initialise firestore
            val db= FirebaseFirestore.getInstance()
        //write name of collection file where document stored
            db.collection("lists")
                .get()
                .addOnCompleteListener {
                    val result:StringBuffer= StringBuffer()
                    if(it.isSuccessful)
                    {
                        //
                        for (document in it.result!!)
                        {
                            result.append(document.data.getValue("email")).append(" ")
                                .append(document.data.getValue("name")).append("\n\n")
                        }
                        txtResult.setText(result)
                    }
                }
    }
}