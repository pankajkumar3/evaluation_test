package com.pankaj.evaluation_test_project.views.loginActivity


import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.google.firebase.auth.FirebaseAuth
import com.pankaj.evaluation_test_project.R
import com.pankaj.evaluation_test_project.databinding.ActivityLoginBinding
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.activity_login.edtPassword
import kotlinx.android.synthetic.main.activity_signup.*

class LoginActivity : AppCompatActivity() {
    private var login:ActivityLoginBinding?=null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        login = DataBindingUtil.setContentView(this, R.layout.activity_login)
        val factory = LoginFactory(this)
        val viewModel = ViewModelProvider(this, factory).get(LoginVM::class.java)
        login?.viewModel=viewModel
        login()
    }
    fun login()
    {
        btnLogin.setOnClickListener {
        when {
            TextUtils.isEmpty(edtEmailorPhone.text.toString().trim { it <= ' ' }) ->
                Toast.makeText(this@LoginActivity, "Please enter email or phone", Toast.LENGTH_SHORT)
                        .show()
            TextUtils.isEmpty(edtPassword.text.toString().trim { it <= ' ' }) ->
                Toast.makeText(
                    this@LoginActivity, "Please enter password", Toast.LENGTH_SHORT)
                        .show()
            else -> {
                val email: String = edtEmailorPhone.text.toString().trim { it <= ' ' }
                val password: String = edtPassword.text.toString().trim { it <= ' ' }
                FirebaseAuth.getInstance().signInWithEmailAndPassword(email, password)
                    .addOnCompleteListener {
                        task->
                        if (task.isSuccessful)
                        {
                            Toast.makeText(this@LoginActivity,"Login Successfully",Toast.LENGTH_SHORT).show()
                        }
                        else
                        {
                            Toast.makeText(this@LoginActivity,"Login not Successful",Toast.LENGTH_SHORT).show()
                        }
                    }
            }
        }
        }
    }
}