package com.pankaj.evaluation_test_project

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.pankaj.evaluation_test_project.views.listActivity.ListActivity
import kotlinx.android.synthetic.main.activity_home.*

class HomeActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        val userId=intent.getStringExtra("user_id")
        val userEmail=intent.getStringExtra("email_id")
        val userName=intent.getStringExtra("user_name")

        txtUserEmail.text="User Eamil - $userEmail"
        txtUserId.text="User id - $userId"
        txtUserName.text="User name - $userName"
        fireStoreValues.setOnClickListener {
            startActivity(Intent(this@HomeActivity,ListActivity::class.java))
            finish()
        }
    }
}