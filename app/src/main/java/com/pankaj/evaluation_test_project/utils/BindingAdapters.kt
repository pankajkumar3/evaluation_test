package com.pankaj.evaluation_test_project.utils

import androidx.databinding.BindingAdapter
import androidx.databinding.ObservableField
import com.hbb20.CountryCodePicker

object BindingAdapters {
    @BindingAdapter(value = ["setSelectedCountryCode"], requireAll = false)
    @JvmStatic
    fun setSelectedCountryCode(countryCodePicker: CountryCodePicker, observableField: ObservableField<Int>) {
        countryCodePicker.setCountryForPhoneCode(observableField.get()!!)
    }
}